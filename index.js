console.log("wew");

// without the use of objects, our students here from before would be organized as follows if we are to record additional information about them.

// spaghetti code - code is not organized enough that it becomes hard to work with

// encapsulation - organize related information (properties) and behavior (methods to belong to a single entity)


// //create student one
// let studentOneName = 'Tony';
// let studentOneEmail = 'starksindustries@mail.com';
// let studentOneGrades = [89, 84, 78, 88];

// //create student two
// let studentTwoName = 'Peter';
// let studentTwoEmail = 'spideyman@mail.com';
// let studentTwoGrades = [78, 82, 79, 85];

// //create student three
// let studentThreeName = 'Wanda';
// let studentThreeEmail = 'scarlettMaximoff@mail.com';
// let studentThreeGrades = [87, 89, 91, 93];

// //create student four
// let studentFourName = 'Steve';
// let studentFourEmail = 'captainRogers@mail.com';
// let studentFourGrades = [91, 89, 92, 93];

// //actions that students may perform will be lumped together
// function login(email){
//     console.log(`${email} has logged in`);
// }

// function logout(email){
//     console.log(`${email} has logged out`);
// }

// function listGrades(grades){
//     grades.forEach(grade => {
//         console.log(grade);
//     })
// }


// Encapsulate the following information into 4 student objects using object literals

let studentOne = {
  name: "Tony",
  email: "starkindustries@mail.com",
  grades: [89, 84, 78, 88],

  // add the functionalities available to a student as object methods
  // keyword "this" refers to the object encapsulating the method where "this" is called
  
  login(){
    console.log(`${this.email} has logged in`);
  },

  logout(email){
    console.log(`${email} has logged out`);
  },

  listGrades(grades){
    this.grades.forEach(grade => {
      console.log(`${this.name}'s quarterly grade averages are: ${grade}`);
    })
  },
 
  computeAve(){
    let sum = 0;
    this.grades.forEach(grade => sum = sum + grade);
    return sum / 4;
  },

  willPass(){
    // return this.computeAve() >= 85 ? true : false
    // syntax of ternary operator : condition ? value if condition true : value if condition if false
    let compute = this.computeAve()
    
    if(compute >= 85) {
      return true;
    }else{
      return false;
    }
  },

  // method that returns true if the student has passed and their average is equal..
  willPassWithHonors(){
    return (this.willPass() && this.computeAve() >= 90) ? true : false
  }
}

console.log(`student one's name is ${studentOne.name}`);

// Mini-Quiz:
/*
    1. What is the term given to unorganized code that's very hard to work with?

    Spaghetti Code

   2. How are object literals written in JS?

   {}

   3. What do you call the concept of organizing information and functionality to belong to an object?
   
   encapsulation

   4. If student1 has a method named enroll(), how would you invoke it?
  
   student1.enroll()

   5. True or False: Objects can have objects as properties.
  
   True

   6. What does the this keyword refer to if used in an arrow function method?
   
   Referes to the object/ if no object - global window object

   7. True or False: A method can have no parameters and still work.

   True

   8. True or False: Arrays can have objects as elements.
   
   True

   9. True or False: Arrays are objects.
   
   True

   10. True or False: Objects can have arrays as properties.

   True
*/

// Mini Activity - encapsulate the proper properties and methods to studentTwo, studentThree and studentFour

let studentTwo = {
  name: "Bony",
  email: "barkindustries@mail.com",
  grades: [82, 75, 86, 94],
  
  login(){
    console.log(`${this.email} has logged in`);
  },

  logout(email){
    console.log(`${email} has logged out`);
  },

  listGrades(grades){
    this.grades.forEach(grade => {
      console.log(`${this.name}'s quarterly grade averages are: ${grade}`);
    })
  },
 
  computeAve(){
    let sum = 0;
    this.grades.forEach(grade => sum = sum + grade);
    return sum / 4;
  },

  willPass(){
    return this.computeAve() >= 85 ? true : false
  },

  willPassWithHonors(){
    return (this.willPass() && this.computeAve() >= 90) ? true : false
  }
};

let studentThree = {
  name: "Pony",
  email: "parkindustries@mail.com",
  grades: [86, 88, 95, 92],
  
  login(){
    console.log(`${this.email} has logged in`);
  },

  logout(email){
    console.log(`${email} has logged out`);
  },

  listGrades(grades){
    this.grades.forEach(grade => {
      console.log(`${this.name}'s quarterly grade averages are: ${grade}`);
    })
  },
 
  computeAve(){
    let sum = 0;
    this.grades.forEach(grade => sum = sum + grade);
    return sum / 4;
  },

  willPass(){
    return this.computeAve() >= 85 ? true : false
  },

  willPassWithHonors(){
    return (this.willPass() && this.computeAve() >= 90) ? true : false
  }
};

let studentFour = {
  name: "Fony",
  email: "farkindustries@mail.com",
  grades: [89, 96, 88, 98],
  
  login(){
    console.log(`${this.email} has logged in`);
  },

  logout(email){
    console.log(`${email} has logged out`);
  },

  listGrades(grades){
    this.grades.forEach(grade => {
      console.log(`${this.name}'s quarterly grade averages are: ${grade}`);
    })
  },
 
  computeAve(){
    let sum = 0;
    this.grades.forEach(grade => sum = sum + grade);
    return sum / 4;
  },

  willPass(){
    return this.computeAve() >= 85 ? true : false
  },

  willPassWithHonors(){
    return (this.willPass() && this.computeAve() >= 90) ? true : false
  }
};

const classOf1A = {
  students: [studentOne, studentTwo, studentThree, studentFour],
  countHonorStudents(){
    let result = 0;
    this.students.forEach(student => {
      if(student.willPassWithHonors()){
        result++
      }
    })
    return result;
  },

  honorsPercentage(){
    let percentage = this.countHonorStudents()
    return (percentage/4)*100
  },

  retrieveHonorStudentInfo(){
    let honored = [];

    this.students.forEach(student => {
      let ave = student.computeAve();
      let honor = {
        email: student.email,
        average: ave
      }
      if(student.willPassWithHonors()){
        honored.push(honor);
      }
    })
    return honored;
  },

  sortHonorStudentsByGradeDesc(){
    let honored = [];

    this.students.forEach(student => {
      let ave = student.computeAve();
      let honor = {
        email: student.email,
        average: ave
      }
      if(student.willPassWithHonors()){
        honored.push(honor);       
      }

      honored.sort((a,b)=>{
        return a.length - b.length;
      });
    })
    return honored;
  }
}

